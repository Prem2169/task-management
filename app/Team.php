<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'leader_id'];

    /**
     * ACCESSORs
     */
    public function getWorkingMembersAttribute()
    {
        return $this->members()->where('status','WORKING')->get();
    }
    public function getVacantMembersAttribute()
    {
        return $this->members()->where('status','VACANT')->get();
    }
    public function getTasksWithPivotsAttribute()
    {
        /**
         * here i will only get the tasks but not with there pivots, so used each get their pivots
         * so we are getting first the collection then making that collection to array.
         * bcoz while using map if we want to access the task so we have to use like this '$tasks[0][0]' and 2nd task as '$tasks[0][0]'
         * so for ease of accessing i am converting them in array, so to use this just acces like $tasks[0] and $tasks[1] and respectively.
         */
        $tasksCollection = $this->tasks()->get()->map(function($task){
            return $task->record;
        });
        foreach($tasksCollection as $task)
        {
            $tasks[] = $task[0];
        }
        $tasks = collect($tasks); // this converts the array to collection. nothing much serious bro
        return $tasks;
    }

    /**
     * RELATIONSHIP METHODS
     */
    public function members(){
        return $this->hasMany(User::class, 'team_id')->where('role', 'member');
    }
    public function leader(){
        return $this->hasOne(User::class, 'id', 'leader_id');
    }
    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
