<?php

namespace App;

use DateTime;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','team_id', 'status', 'percentage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * ACCESSORS
     */
    public function getAvatarAttribute()
    {
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }
    public function getCompletedTasksAttribute(){
        if($this->role == 'leader')
        {
            return $this->team->tasks()->where('status', 'RESOLVED')->get();
        }
        if($this->role == 'member')
        {
            return $this->tasks()->wherePivot('status', 'RESOLVED')->get();
        }
        return 'Role Mismatch';
    }
    public function getFailedTasksAttribute(){
        if($this->role == 'leader')
        {
            return $this->team->tasks()->where('status', 'UNRESOLVED')->get();
        }
        if($this->role == 'member')
        {
            return $this->tasks()->wherePivot('status', 'UNRESOLVED')->get();
        }
        return 'Role Mismatch';
    }

    /**
     * HELPER METHODS
     */

    /**
     * This function will calculate the percentage rate of the member as well as leader
     */
    public function updateUserStatus()
    {
        $getUserStatus = $this->status;
        if($this->tasks->count() > 0)
        {
            if($getUserStatus == 'VACANT')
            {
                $this->update(['status'=>'WORKING']);
            }
        }
        else
        {
            if($getUserStatus == 'WORKING')
            {
                $this->update(['status'=>'VACANT']);
            }
        }
    }
    public function calculatePercentage()
    {

        /**
         * PERCENTAGE VALUES FROM 100%
         * 1 : FOR MEMBER :
         *          successTasks = 50%;
         *          successTasksReport = 25%;
         *          reassign = 25%;
         * 2 : FOR LEADER :
         *          successTasks = 50%;
         *          successTasksReport = 20%;
         *          reassign = 20%;
         *          giveup = 10%;
         *
         * note : first i am calculating every condition in terms 100% then i am converting it to theri respected percentages mentioned above
         *
         * **************** REASSIGN CALCULATION ************
         * reassign rate some examples of calculation
         * formula : ($totalTasks->count() / ($reassignCount + $totalTasks->count()) ) * 100
         * 3 case :
         * 1 : 5tasks and 0 reassign and result we want is  100%
         *      sol : as per formula
         *              = ( 5 / (0+5) ) * 100
         *              = (5 / 5 ) * 100
         *              = 100%
         * 2 : 5tasks and 2 reassign and result we want is something and comparabel to above result
         *      sol : as per formula
         *              = ( 5 / (2+5) ) * 100
         *              = (5 / 7 ) * 100
         *              = 71.42%
         * 3 : 5tasks and 8 reassign and result we want is something and comparabel to above result
         *      sol : as per formula
         *              = ( 5 / (8+5) ) * 100
         *              = (5 / 13 ) * 100
         *              = 38.46%
         *
         * NOTE : these examples are based on the formula written at the star of this comment
         *
         * **************** SUCCESS TASKS REPORT CALCULATION ************
         * 1 : first iam calculating the time given
         * 2 : then iam calculating totalTimeTookToCompleteTask
         * 3 : then im dividing (2 by 1), and then multiply by 100 so that we get in terms of 100%
         * 4 : then if the successRate as given 20%
         *      so : (20 / totalTaskResolved)
         */

        //SOME CONSTANTS FOR CONDITION PERCENTAGE as mentioned in above comments
        /**
         * for leader : 50 + 20 + 20 + 10
         * for member : 50 + 25 + 25
         */
        //general percentages (i.e for members and employess are same)
        $RESOLVED_BY_ASSIGNED_PERCENTAGE = 50 / 100;
        //for leader
        $SUCCESS_TASKS_PERCENTAGE_FOR_LEADER = 20 / 100;
        $REASSIGN_TASKS_PERCENTAGE_FOR_LEADER = 20 / 100;
        $GIVEUP_TASKS_PERCENTAGE_FOR_LEADER = 10 / 100;
        //for member
        $SUCCESS_TASKS_PERCENTAGE_FOR_MEMBER = 25 / 100;
        $REASSIGN_TASKS_PERCENTAGE_FOR_MEMBER = 25 / 100;

        $totalTasks = null;
        $tasksCompleted = $this->completed_tasks;
        $tasksFailed = $this->failed_tasks;

        if($this->role == 'leader')
        {
            $totalTasks = $this->team->tasks_with_pivots;
            $reassignCount = 0;
            $giveupCount = 0;

            //calculating reassigncounts and giveupcounts
            foreach($this->team->tasks as $task)
            {
                foreach($task->record as $pivotRecord)
                {
                    $reassignCount += $pivotRecord->pivot->reassign_count;
                    if($pivotRecord->pivot->is_giveup)
                    {
                        $giveupCount ++;
                    }
                }
            }

            //calculating success percentage for each resolved task
            if($tasksCompleted->count() != 0)
            {
                $singleSuccessTaskPercentage = round( ($SUCCESS_TASKS_PERCENTAGE_FOR_LEADER / $tasksCompleted->count()), 3);
            }
        }
        elseif($this->role == 'member')
        {
            //here i will get the task with pivots
            $totalTasks = $this->tasks;
            $reassignCount = 0;

            //calculating reassigncounts
            foreach($totalTasks as $task)
            {
                $reassignCount += $task->pivot->reassign_count;
            }

            //calculating success percentage for each resolved task
            if($tasksCompleted->count() != 0)
            {
                $singleSuccessTaskPercentage = round( ($SUCCESS_TASKS_PERCENTAGE_FOR_MEMBER / $tasksCompleted->count()), 3);
            }
        }
        $totalTasksCount = $totalTasks->count();
        $totalPercentage = 0;

        //if there are tasks then only i will calculate otherwise i will not calculate
        if($totalTasksCount > 0)
        {

            // Calculating condition rates (without percentage conditions)
            $reassignRate = ($totalTasksCount / ($reassignCount + $totalTasksCount) ) * 100;
            $resolvedTasksByTotalTasksRate = ($tasksCompleted->count() / ($tasksCompleted->count() + $tasksFailed->count() )) * 100;

            //this for loopwill calculate all the resolved Tasks success percentage and will divide and add according to percentage for leader or person mentioned above
            $totalCompletedTasksSuccessInPercentage = 0;
            foreach($tasksCompleted as $task)
            {
                $totalTimeGiven = strtotime($task->deadline) - strtotime($task->created_at);
                $totalTimeTookToCompleteTask = strtotime($task->completed_at) - strtotime($task->created_at);
                $taskSuccessRate = ($totalTimeTookToCompleteTask / $totalTimeGiven) * 100;
                $totalCompletedTasksSuccessInPercentage += ($taskSuccessRate * $singleSuccessTaskPercentage);
            }

            //Percentage Calculation according to leader or member
            $resolvedTasksByTotalTasksPercentage = $resolvedTasksByTotalTasksRate * $RESOLVED_BY_ASSIGNED_PERCENTAGE;
            $totalPercentage = 0;
            if($this->role == 'leader')
            {
                $reassignTasksPercentage = $reassignRate * $REASSIGN_TASKS_PERCENTAGE_FOR_LEADER;

                $taskCountIntoTaskMemberCount = ($totalTasksCount * $this->team->members->count());
                $giveupTasksRate = ($taskCountIntoTaskMemberCount / ($taskCountIntoTaskMemberCount + $giveupCount) ) * 100;
                $giveupTasksPercentage = $giveupTasksRate * $GIVEUP_TASKS_PERCENTAGE_FOR_LEADER;

                $totalPercentageInFloat = $resolvedTasksByTotalTasksPercentage +
                                    $totalCompletedTasksSuccessInPercentage +
                                    $reassignTasksPercentage +
                                    $giveupTasksPercentage;
                $totalPercentage = round($totalPercentageInFloat, 3);
            }
            elseif($this->role == 'member')
            {
                $reassignTasksPercentage = $reassignRate * $REASSIGN_TASKS_PERCENTAGE_FOR_MEMBER;

                $totalPercentageInFloat = $resolvedTasksByTotalTasksPercentage +
                                    $totalCompletedTasksSuccessInPercentage +
                                    $reassignTasksPercentage;
                $totalPercentage = round($totalPercentageInFloat, 3);
            }
        }

        // dd(gettype($totalPercentage));

        /**
         * This method is called in :
         *      CheckAndUpdateTasksDeadline Middleware
         *      CheckTaskDeadline Middleware
         *      TaskController@approve
         *      TaskController@giveup
         */
        if($this->role == 'member')
        {
            $this->team->leader->calculatePercentage();
        }
        $this->update(['percentage' => $totalPercentage]);
    }
    public function bestEmployee()
    {
        $employees = $this->team->members()->orderBy('percentage', 'DESC')->get();
        return $employees[0];
    }
    public function bestLeader()
    {
        $leaders = User::where('role', 'leader')->orderBy('percentage', 'DESC')->get();
        return $leaders[0];
    }


    /**
     * RELATIONSHIPS METHODS
     */
    /**
     * If u want leader ke created task so use '$user->team->tasks'
     */
    public function tasks()
    {
        if($this->role == 'member')
        {
            return $this->belongsToMany(Task::class,'member_task', 'member_id', 'task_id')->withPivot('member_id', 'status', 'reassign_count', 'is_giveup', 'completed_at  ')->withTimestamps();
        }
        return 'Role Mismatch';
    }
    public function team()
    {
        // if($this->team_id != null)
        // {
            return $this->belongsTo(Team::class, 'team_id', 'id');
        // }
    }
}
