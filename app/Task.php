<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'team_id', 'deadline', 'status', 'completed_at'];

    /**
     * MUTATORTS
     */
    public function setTaskPivotStatusAttribute($status)
    {
        $this->record()->latest()->get()[0]->pivot->update(['status'=>$status]);
    }

    /**
     * ACCESSORS
     */
    public function getTaskPivotStatusAttribute()
    {
        return $this->record()->latest()->get()[0]->pivot->status;

    }
    public function getTaskPivotAttribute()
    {
        return $this->record[0]->pivot;
    }

    /**
     * HELPER METHODS
     */
    public function deadlineUpdate(){
        /**
         * This function will only call when the task's deadline is over
         * this function will set the status of both the tables i.e 'tasks' as well as pivot table i.e 'member_task' to 'UNRESOLVED'
         * Also if the member of the task has no more assigned tasks then the status will we set to VACANT.
         */
        $this->task_pivot->update(['status'=>'UNRESOLVED']);
        $this->update(['status'=> 'UNRESOLVED']);
        if($this->member[0]->tasks->where('status','ASSIGNED')->count() == 0)
        {
            $this->member[0]->update(['status'=> 'VACANT']);
        }
    }

    /**
     * RELATION SHIP METHODS
     */
    public function team(){
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }
    public function member(){
        /**
         * Note to access the this member u have to use
         *      '$task->member[0]->name'
         *          instead of
         *      '$task->member->name'
         *  bcoz as we have used belongsToMany it is giving the collection, but inside that collection there is only one member
         *
         * NOTE: If one task has been assigned to many members, so $task->member[0] will give the latest member or the member to which thi task was assigned last
         *      I have used this 'member[0]' at mnay places, meaning that the current member of the task is member[0].
         *      thats why i have used the orderby in the member relation
         */
        return $this->belongsToMany(User::class,'member_task', 'task_id', 'member_id')->withPivot('member_id', 'status')->withTimestamps()->orderBy('pivot_created_at', 'DESC');
    }
    public function record(){
        return $this->belongsToMany(Task::class,'member_task','task_id')->withPivot('member_id', 'status', 'completed_at', 'reassign_count', 'is_giveup')->withTimestamps()->orderBy('pivot_created_at', 'DESC');
    }
}
