<?php

namespace App\Http\Requests\tasks;

use Illuminate\Foundation\Http\FormRequest;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->member || $this->randomTaskAssignToMemberCheckbox)
        {
            return auth()->check();
        }
        else
        {
            //Session storage
            session()->flash('error', 'Please Select checkbox   OR  any one member from the list');
            return redirect()->back();
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->member)
        {
            return [
                'name' => 'required|unique:tasks,name' . $this->id,
                'deadline' => 'required|date',
                'member' => 'required'
            ];
        }
        else
        {
            return [
                'name' => 'required|unique:tasks,name' . $this->id,
                'deadline' => 'required|date',
                'randomTaskAssignToMemberCheckbox' => 'required'
            ];
        }
    }
}
