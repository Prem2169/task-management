<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->task->deadline < Carbon::now())
        {
            //Session storage
            session()->flash('error', 'Task deadline exceeds');

            //redirect
            return redirect(route('tasks.index'));
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * If the name comes so we know that this updaterequest is called from the edit task not tfrom the assign task
         */
        if($this->name)
        {
            return [
                'name' => 'required|unique:tasks,name' . $this->id,
                'deadline' => 'date',
            ];
        }
        return [
            'deadline' => 'date',
            'member' => 'required'
        ];

    }
}
