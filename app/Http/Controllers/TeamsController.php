<?php

namespace App\Http\Controllers;

use App\Http\Requests\teams\CreateTeamRequest;
use App\Http\Requests\teams\UpdateTeamRequest;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == 'admin')
        {
            $teams = Team::paginate(10);
        }
        else
        {
            $teams = [];
            $teams[0] = auth()->user()->team;
        }
        // dd($teams);
        return view('teams.index', compact(
            'teams'
        ));
    }

    public function create()
    {
        $members = User::where('team_id', null)->get();
        return view('teams.create', compact(
            'members'
        ));
    }

    public function store(CreateTeamRequest $request)
    {
        // dd($request->members);

        //Create team
        $team = Team::create([
            'name' => $request->name,
            'leader_id' => $request->leader
        ]);

        //Assign the team_id in the users table for leaders as well as members
        User::find($request->leader)->update(['team_id'=>$team->id, 'role'=>'leader']);

        foreach($request->members as $member){
            User::find($member)->update(['team_id' => $team->id, 'role'=>'member']);
        }

        //Session storage
        session()->flash('success', 'Team Created Successfully');

        //redirect
        return redirect(route('teams.index'));

    }

    public function show(Team $team)
    {
        //
    }

    public function edit(Team $team)
    {
        $members = User::where('team_id', $team->id)->OrWhere('team_id', null)->get();
        return view('teams.edit', compact(
            'team',
            'members'
        ));
    }

    public function update(UpdateTeamRequest $request, Team $team)
    {
        // dd($request->members);
        $team->leader()->update(['team_id' => null]);
        $team->update([
            'name' => $request->name,
            'leader_id' => $request->leader
        ]);
        $team->leader()->update(['team_id' => $team->id, 'role'=>'leader']);

        //should have make accessor for workingMembers from team as well as accessor for vacantMembers
        if($team->vacant_members)
        {
            foreach($team->vacant_members as $member )
            {
                $member->update(['team_id' => null]);
            }
        }
        foreach($request->members as $member )
        {
            User::find($member)->update(['team_id' => $team->id, 'role'=>'member']);
        }

        //Session storage
        session()->flash('success', 'Team Updated Successfully');

        //redirect
        return redirect(route('teams.index'));

    }

    public function destroy(Team $team)
    {
        //
    }
}
