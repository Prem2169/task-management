<?php

namespace App\Http\Controllers;

use App\Http\Requests\tasks\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAndUpdateTasksDeadline')->only('index');
        $this->middleware('checkTaskDeadline')->only('edit', 'resolve', 'giveup', 'reassign');
    }

    public function index()
    {
        // User::where('role', '!=', 'admin')->where('team_id', '!=', null)->get()->each(function($user){
        //     $user->calculatePercentage();
        // });
        if(auth()->user()->role == 'admin')
        {
            $tasks = Task::paginate(10);
        }
        elseif(auth()->user()->role == 'leader')
        {
            $tasks = auth()->user()->team->tasks()->paginate(10);
        }
        elseif(auth()->user()->role == 'member')
        {
            $tasks = auth()->user()->tasks()->paginate(10);
        }
        return view('tasks.index', compact(
            'tasks'
        ));
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(CreateTaskRequest $request)
    {
        $member = null;
        if($request->member)
        {
            $member = $request->member;
        }
        else
        {
            $member = auth()->user()->team->members()->pluck('id')->random();
        }
        $task = Task::create([
            'name' => $request->name,
            'team_id' => auth()->user()->team->id,
            'status' => 'ASSIGNED',
            'deadline' => $request->deadline
        ]);

        $task->record()->attach($member, ['status' => 'ASSIGNED', 'member_id' => $member]);

        //Session storage
        session()->flash('success', 'Task Created Successfully');

        //redirect
        return redirect(route('tasks.index'));

    }

    public function show(Task $task)
    {
        //
    }

    public function edit(Task $task)
    {
        $members = auth()->user()->team->members;
        $action = 'edit';
        return view('tasks.edit', compact(
            'task',
            'action',
            'members'
        ));
    }

    public function update(UpdateTaskRequest $request, Task $task)
    {
        if(isset($request->name) != null)
        {
            //If we came inside this . its meanss that this action is edit
            $task->update(['name' => $request->name, 'deadline' => $request->deadline]);

            //Session storage
            session()->flash('success', 'Task Updated Successfully');
        }
        else
        {
            //If we came inside this . its meanss that this action is 'assign'
            $member = User::find($request->member);
            $task->update(['deadline' => $request->deadline]);
            $task->record()->attach($member->id, ['status' => 'ASSIGNED', 'member_id' => $member->id]);
            $member->updateUserStatus();

            //Session storage
            session()->flash('success', 'Task Assigned Successfully');
        }

        //redirect
        return redirect(route('tasks.index'));
    }

    public function destroy(Task $task)
    {
        //
    }

    public function approve(Task $task)
    {
        $completedDate = Carbon::createFromFormat('Y-m-d H:i:s', $task->task_pivot->completed_at );
        $task->task_pivot->update(['status' => 'RESOLVED']);
        $task->update(['status'=>'RESOLVED', 'completed_at'=>$completedDate]);
        $task->member[0]->calculatePercentage();
        $task->member[0]->updateUserStatus();
        return redirect()->back();
    }
    public function reassign(Task $task)
    {
        $task->task_pivot->update(['status' => 'ASSIGNED', 'completed_at' => null, 'reassign_count' => $task->task_pivot->reassign_count + 1]);
        return redirect()->back();
    }
    public function resolve(Task $task)
    {
        $task->task_pivot->update(['status' => 'REQUEST_RESOLVE', 'completed_at' => Carbon::now()]);
        return redirect()->back();
    }
    public function giveup(Task $task)
    {
        $task->task_pivot->update(['status' => 'UNRESOLVED', 'is_giveup' => 1]);
        if($task->member->count() == $task->team->members->count())
        {
            $task->update(['status' => 'UNRESOLVED']);
        }
        $task->member[0]->calculatePercentage();
        $task->member[0]->updateUserStatus();
        return redirect()->back();
    }
    public function assign(Task $task)
    {
        if($this->authorize('assign', $task))
        {
            $members = auth()->user()->team->members;
            $action = 'assign';
            return view('tasks.edit', compact(
                'task',
                'action',
                'members'
            ));
        }
    }
}
