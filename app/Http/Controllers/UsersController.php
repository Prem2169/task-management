<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == 'admin')
        {
            $users = User::paginate(10);
        }
        else
        {
            $users = auth()->user()->team->members()->paginate(10);
        }
        return view('users.index', compact(
            'users'
        ));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        //
    }

    public function update(Request $request, User $user)
    {
        //
    }

    public function destroy(User $user)
    {
        //
    }
}
