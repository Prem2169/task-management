<?php

namespace App\Http\Middleware;

use App\Task;
use Carbon\Carbon;
use Closure;

class CheckAndUpdateTasksDeadline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $deadlineTasks = Task::where('deadline', '<', Carbon::now())->where('status', '==', 'ASSIGNED')->get();
        foreach($deadlineTasks as $task)
        {
            $task->deadlineUpdate();
            $task->member[0]->calculatePercentage();
            $task->member[0]->updateUserStatus();

        }
        return $next($request);
    }
}
