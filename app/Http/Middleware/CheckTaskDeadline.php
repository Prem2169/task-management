<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class CheckTaskDeadline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->task->deadline < Carbon::now() && $request->task->status == 'ASSIGNED')
        {
            //Session storage
            session()->flash('error', 'Task Deadline exceeds');
            $request->task->deadlineUpdate();
            $request->task->member[0]->calculatePercentage();
            $request->task->member[0]->updateUserStatus();

            return redirect()->back();
        }
        return $next($request);
    }
}
