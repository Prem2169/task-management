<?php

namespace App\Policies;

use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class TasksPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    }
    public function view(User $user, Task $task)
    {
        //
    }
    public function create(User $user)
    {

        return $user->role == 'leader';
    }
    public function update(User $user, Task $task)
    {
        return $user->role == 'leader' &&
                $task->status == 'ASSIGNED' &&
                $task->task_pivot_status == 'ASSIGNED';
    }
    public function approveOrReassign(User $user, Task $task)
    {
        return $user->role == 'leader' &&
                $task->task_pivot_status == 'REQUEST_RESOLVE';
    }
    public function resolveOrGiveup(User $user, Task $task)
    {
        return $user->role == 'member' &&
                $task->task_pivot_status == 'ASSIGNED';
    }
    public function assign(User $user, Task $task)
    {

        return $user->role == 'leader' &&
                $task->status == 'ASSIGNED' &&
                $task->task_pivot_status == 'UNRESOLVED'  &&
                $task->member->count() != $task->team->members->count();
    }
    public function delete(User $user, Task $task)
    {
        //
    }
    public function restore(User $user, Task $task)
    {
        //
    }
    public function forceDelete(User $user, Task $task)
    {
        //
    }
}
