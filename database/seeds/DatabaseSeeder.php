<?php

use App\Task;
use App\Team;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        //creating users
        factory(User::class, 1)->create()->each(function($user){
            $user->name = 'Prem Lalchandani';
            $user->email = 'prem.lalchandani.46@gmail.com';
            $user->password = '$2y$10$d5/Xedmm/EBi6oceShOaM.m8bpuUq7q/.qtehWUz33xgjYX3OHR0a';
            $user->role = 'admin';
            $user->team_id = 0;
            $user->save();
        });
        factory(User::class, 16)->create();

        //creating 3 teams (with member)
        factory(Team::class,3)->create()->each(function($team){
            User::find($team->leader_id)->update(['team_id'=>$team->id, 'role'=>'leader']);

            //forloop for creating members of team
            for($i=0; $i<3; $i++){
                $user = User::find(User::where('team_id',null)->pluck('id')->random());
                $user->update(['team_id'=>$team->id, 'role'=>'member']);
            }
        });

        //creating tasks (task will be created by any leader of any team and task will be assigned to the respected team member)
        factory(Task::class, rand(20,30))->create()->each(function($task){
            $randomTeamMember = $task->team->members->where('role','!=','leader')->random();
            /**
             * this array ihave made bocz if the task is resolved so i have to set the completed also in pivot table
             * so therefore i had first made an array and if the task is resolved so i will copy the completed of task into pivot table
             * If not resolved so im not setting the completed_at and as while making migration the completed_at is set to null by default
             */
            $pivotArray = [
                'status' => $task->status,
                'member_id' => $randomTeamMember->id
            ];
            if($task->status == "RESOLVED")
            {
                $pivotArray['completed_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $task->completed_at);
            }

            $task->record()->attach($randomTeamMember, $pivotArray);
            $randomTeamMember->update(['status'=>'WORKING']);
        });
        User::where('role', '!=', 'admin')->get()->each(function($user){
            $user->calculatePercentage();
        });
    }
}
