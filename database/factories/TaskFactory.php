<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use App\Team;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $team_id = Team::pluck('id')->random();
    $statusArray = ['ASSIGNED', 'RESOLVED', 'UNRESOLVED'];

    $returnArray = [
        'name' => $faker->sentence(rand(5,10)),
        'team_id'=>$team_id,
        'status'=>$statusArray[rand(0,2)],
        'deadline'=>Carbon::now()->addDays(rand(3,5))->hour(rand(1,24)),
    ];

    /**
     * If the task is resolved set the 'completed_at' else not
     */
    if($returnArray['status'] == 'RESOLVED')
    {
        $returnArray['completed_at'] = Carbon::now()->addDays(rand(1,3))->hour(rand(1,24))->minute(rand(1,60));
    }

    return $returnArray;
});
