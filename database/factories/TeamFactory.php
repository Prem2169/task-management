<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Team;
use App\User;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    $leader = User::where('team_id',null)->where('role', '!=', 'leader')->pluck('id')->random();
    return [
        'name'=>$faker->sentence(rand(3,5)),
        'leader_id'=>$leader
    ];
});
