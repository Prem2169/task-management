<?php

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('team_id');
            $table->string('status')->default('ASSIGNED');
            $table->timestamp('completed_at')->nullable();
            $table->timestamp('deadline')->default(
                Carbon::now()->addDays(rand(3,5))->hour(rand(1,24))
            );
            //note as i have added the APP_TIMEZONE in .env file thats why i am getting the 'today' as current date, otherwise it will give the unix timestap starting date
             $table->timestamps();

            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
