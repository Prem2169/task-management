<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();

Route::get('/', function () { return view('home'); })->name('home');
Route::get('/login', function(){ return view('authentication/login'); })->name('login');
Route::get('/register', function(){ return view('authentication/register'); })->name('register');

Route::middleware(['auth'])->group(function () {
    Route::resource('users', 'UsersController');
    Route::resource('teams', 'TeamsController');
    Route::resource('tasks', 'TasksController');
    //checkTaskDeadline middleware is applied inside the TasksController

    Route::get('tasks/{task}/approve', 'TasksController@approve')->name('task.approve');
    Route::get('tasks/{task}/reassign', 'TasksController@reassign')->name('task.reassign');
    Route::get('tasks/{task}/resolve', 'TasksController@resolve')->name('task.resolve');
    Route::get('tasks/{task}/giveup', 'TasksController@giveup')->name('task.giveup');
    Route::get('tasks/{task}/assign', 'TasksController@assign')->name('task.assign');
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
