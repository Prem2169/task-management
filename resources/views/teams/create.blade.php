@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Add Team</div>
        <div class="card-body">
            <form action="{{ route('teams.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"
                            value="{{ old('name') }}"
                            class="form-control"
                            name="name" id="name">
                    @error('name')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="leader">Leader</label>
                        <select name="leader" id="leader" class="form-control select2" onchange="leaderChange({{ $members }})">
                            <option value="-1" disabled selected>Select ... </option>
                            @foreach ($members as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                        @error('leader')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="members">Members</label>
                        <select name="members[]" id="members" class="form-control select2" multiple >
                            {{-- @foreach ($members as $member) --}}
                                <option value="-1" disabled>Select Leader first</option>
                            {{-- @endforeach --}}
                        </select>
                        @error('members')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Team</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function(){

        });
        $('.select2').select2({
            placeholder : 'Select..'
        });

        function leaderChange($members){
            let leaderValue = $('#leader').val();

            let updatedMembers = $members.filter(member => member.id != leaderValue);

            var str = "";
            for (var member of updatedMembers) {
                str += `<option value="${member.id}">${member.name}</option>`;
            }
            document.getElementById('members').innerHTML = str;
        }
    </script>
@endsection

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
