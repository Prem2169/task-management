@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('teams.create') }}" class="btn btn-primary">Add Team</a>
    </div>

    <div class="card">

        <div class="card-header">Teams</div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <th>Name</th>
                        <th>Leader</th>
                        <th>Members</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        @foreach ($teams as $team)
                            <tr>
                                <td>
                                    {{ $team->name }}
                                </td>
                                <td>
                                    {{ $team->leader->name }}
                                </td>
                                <td>|
                                    @foreach ($team->members()->where('role','member')->get() as $member)
                                        {{ $member->name }} |
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('teams.edit', $team->id) }}" class="btn btn-outline-primary btn-sm">Edit</a>
                                    <a href=""
                                        class="btn btn-outline-danger btn-sm"
                                        {{-- onclick="displayModalForm({{ $team }})" --}}
                                        data-toggle="modal"
                                        data-target="#deleteModal">Trash</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
            </div>
        </div>

        <div class="card-footer">
            {{ is_array($teams) ? '' : $teams->links() }}
        </div>
    </div>

@endsection
