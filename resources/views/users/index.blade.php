@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-end mb-3">
        <a href="{{ route('register') }}" class="btn btn-primary">Add User</a>
    </div>

    <div class="card">

        <div class="card-header">Users</div>

        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Percentage</th>
                        <th>Type</th>
                        <th>Team</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        {{-- {{ dd($users) }} --}}
                        @foreach ($users as $user)
                            <tr>
                                <td>
                                    <img src="{{ $user->avatar }}" alt="">
                                </td>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    {{ $user->status }}
                                </td>
                                <td>{{ $user->percentage }}</td>
                                <td>
                                    {{ $user->role }}
                                </td>
                                <td>
                                    @if ($user->team_id)
                                        {{ $user->team->name }}
                                    @endif
                                </td>
                                <td>
                                    {{-- @if (!$user->isAdmin()) --}}
                                    <a href=""
                                    class="btn btn-outline-danger btn-sm"
                                    onclick="displayModalForm({{ $user }})"
                                    data-toggle="modal"
                                    data-target="#makeAdminModal">Make Admin</a>
                                    {{-- @endif --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $users->links() }}
        </div>
    </div>

    <!-- MAKEADMIN Modal -->
    <div class="modal fade" id="makeAdminModal" tabindex="-1" role="dialog" aria-labelledby="makeAdminModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="makeAdminModal">MakeAdmin User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="" method="POST" id="makeAdminForm">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    Are your sure you want to Make Admin
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">MakeAdmin User</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <!-- END MakeAdmin Modal-->

@endsection

@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($user)
        {
            var url = '/users/' + $user.id + '/make-admin';
            $('#makeAdminForm').attr('action', url);
        }
    </script>
@endsection
