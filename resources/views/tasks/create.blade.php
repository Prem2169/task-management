@extends('layouts.app')

@section('content')
    <div class="card ">
        <div class="card-header">Add Task</div>
        <div class="card-body">
            <form action="{{ route('tasks.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="form-group col-md-5">
                        <label for="name">Name</label>
                        <input type="text"
                                value="{{ old('name') }}"
                                class="form-control"
                                name="name" id="name">
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="col-md-1"></div> {{-- Extra div here --}}

                    <div class="form-group col-md-4">
                        <label for="deadline">Deadline</label>
                        <input type="date"
                                value="{{ old('deadline') }}"
                                class="form-control"
                                name="deadline" id="deadline">
                        @error('deadline')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group col-md-5">
                        <label for="member">Member</label>
                        <select name="member" id="member" class="form-control select2" >
                                <option value="" disabled selected>Select ..</option>
                            @foreach (auth()->user()->team->members()->where('role', '!=', 'leader')->get() as $member)
                                <option value="{{ $member->id }}">{{ $member->name }}</option>
                            @endforeach
                        </select>
                        @error('member')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="col-md-1"></div> {{-- Extra div here --}}

                    <div class="form-group pl-4 pt-4 col-md-4">
                        <div class="form-check">
                            <label class="form-check-label">
                            <input type="checkbox" class="form-check-input"
                                    name="randomTaskAssignToMemberCheckbox" id="randomTaskAssignToMemberCheckbox"
                                    value="randomTaskAssignToMemberCheckboxSelected"
                                    {{-- checked --}}
                                    onchange="checkboxChange()">
                            Select for randomly task assign.
                            </label>
                        </div>
                        @error('randomTaskAssignToMemberCheckbox')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Task</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        flatpickr("#deadline", {
            enableTime: true
        });
        $('.select2').select2({
            placeholder : 'Select..'
        });

        function checkboxChange(){
            var checkBox = document.getElementById('randomTaskAssignToMemberCheckbox');
            var selectElement = document.getElementById('member');

            if(checkBox.checked){
                selectElement.disabled = true;
            }
            else{
                selectElement.disabled = false;
            }
        }
    </script>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
