@extends('layouts.app')

@section('content')
    <div class="card ">
        <div class="card-header">{{ $action }} Task</div>
        <div class="card-body">
            <form action="{{ route('tasks.update', $task) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="form-group col-md-5">
                        <label for="name">Name</label>
                        <input type="text"
                                value="{{ old('name', $task->name) }}"
                                class="form-control"
                                name="name" id="name"
                                @if ($action == 'assign')
                                    disabled
                                @endif
                                >
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="col-md-1"></div> {{-- Extra div here --}}

                    <div class="form-group col-md-4">
                        <label for="deadline">Deadline</label>
                        <input type="text"
                                value="{{ $task->deadline }}"
                                class="form-control"
                                name="deadline" id="deadline">
                        @error('deadline')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group col-md-5">
                        <label for="member">Member</label>
                        <select name="member" id="member" class="form-control select2"
                            @if ($action == 'edit')
                                disabled
                        >
                            <option value="{{ $task->member[0]->id }}" disabled selected>{{ $task->member[0]->name }}</option>
                            @else
                        >
                                <option value="" disabled selected>Select ..</option>
                                @foreach ($members as $member)
                                    <option value="{{ $member->id }}"
                                        @foreach ($task->member as $task_member)
                                            @if ($member->name == $task_member->name)
                                                disabled
                                            @endif
                                        @endforeach
                                        >
                                        {{ $member->name }}
                                    </option>
                                @endforeach
                            @endif

                        </select>
                        @error('member')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">{{ $action }} Task</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        flatpickr("#deadline", {
            enableTime: true
        });
        $('.select2').select2({
            placeholder : 'Select..'
        });

        function checkboxChange(){
            var checkBox = document.getElementById('randomTaskAssignToMemberCheckbox');
            var selectElement = document.getElementById('member');

            if(checkBox.checked){
                selectElement.disabled = true;
            }
            else{
                selectElement.disabled = false;
            }
        }
    </script>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
