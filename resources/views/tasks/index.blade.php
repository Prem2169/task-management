@extends('layouts.app')

@section('content')

    @can('create', App\Task::class)
        <div class="d-flex justify-content-end mb-3">
            <a href="{{ route('tasks.create') }}" class="btn btn-primary">Add Task</a>
        </div>
    @endcan

    <div class="card">

        <div class="card-header">Tasks</div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <th>Name</th>
                        <th>Team</th>
                        @if (auth()->user()->role != 'member')
                            <th>Assigned To </th>
                        @endif
                        <th>reassign_count</th>
                        <th>is_giveup</th>
                        <th>Status</th>
                        <th>Deadline</th>
                        <th>Actions</th>
                    </thead>
                    <tbody>
                        {{-- {{ dd($tasks) }} --}}
                        @foreach ($tasks as $task)
                            <tr>
                                <td>
                                    {{ $task->name }}
                                </td>
                                <td>
                                    {{ $task->team->name }}
                                </td>
                                @if (auth()->user()->role != 'member')
                                    <td>
                                        {{ $task->member[0]->name }}
                                    </td>
                                @endif
                                <td>
                                    {{ $task->task_pivot->reassign_count }}
                                </td>
                                <td>
                                    {{ $task->task_pivot->is_giveup }}
                                </td>
                                <td>
                                    @if (auth()->user()->role == 'member')
                                        {{ $task->task_pivot_status }}
                                    @else
                                        {{ $task->status }}
                                    @endif
                                </td>
                                <td>
                                    {{ $task->deadline }}
                                </td>
                                {{-- actions:START --}}
                                <td>
                                    @can('update', $task)
                                        <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-outline-primary btn-sm">Edit</a>
                                    @elsecan('approveOrReassign', $task)
                                        <a href="{{ route('task.approve', $task->id) }}" class="btn btn-outline-success btn-sm">Approve</a>
                                        <a href="{{ route('task.reassign', $task->id) }}" class="btn btn-outline-primary btn-sm">Reassign</a>
                                    @elsecan('resolveOrGiveup', $task)
                                        <a href="{{ route('task.resolve', $task->id) }}" class="btn btn-outline-primary btn-sm">Resolve</a>
                                        <a href="{{ route('task.giveup', $task->id) }}" class="btn btn-outline-danger btn-sm">Give up</a>
                                    @elsecan('assign', $task)
                                        <a href="{{ route('task.assign', $task->id) }}" class="btn btn-outline-danger btn-sm">Assign</a>
                                    @else
                                        {{-- {{ date("Y-m-d / h:i:sa", strtotime($task->deadline))  }} --}}
                                        {{ 'No Actions' }}
                                        {{-- @if (\Carbon\Carbon::now() > $task->deadline)
                                        @endif --}}
                                    @endcan
                                </td>
                                {{-- actions:END --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-footer">
            {{ $tasks->links() }}
        </div>
    </div>

@endsection
